package com.hbo.ut.repository;

import com.hbo.ut.domain.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PatientRepository extends JpaRepository<Patient, Integer> {
    Optional<Patient> findBySecurityNumber(Integer securityNumber);
}
