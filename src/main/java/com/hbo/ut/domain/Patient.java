package com.hbo.ut.domain;

import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Patient implements Serializable {

    @Id
    @GeneratedValue
    private Integer id;
    private Integer securityNumber;
    private String firstname;
    private String lastname;
    private LocalDate birthday;
    @OneToMany(mappedBy = "patient", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<Appointment> appointments = new ArrayList<>();
}
