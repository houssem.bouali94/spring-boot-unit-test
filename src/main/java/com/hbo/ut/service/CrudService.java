package com.hbo.ut.service;

import java.util.Collection;

public interface CrudService<T, ID> {

    ID save(T t);

    ID delete(ID id);

    T findById(ID id);

    Collection<T> findAll();
}
