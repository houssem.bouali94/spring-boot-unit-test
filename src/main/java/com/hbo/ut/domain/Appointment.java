package com.hbo.ut.domain;

import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Appointment implements Serializable {

    @Id
    @GeneratedValue
    private Integer id;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private String medicalService;
    @ManyToOne
    @JoinColumn(name = "patient_id")
    private Patient patient;
}
