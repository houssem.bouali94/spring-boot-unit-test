package com.hbo.ut.repository;

import com.hbo.ut.domain.Appointment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AppointmentRepository extends JpaRepository<Appointment, Integer> {
    List<Appointment> findByMedicalService(String medicalService);
}
