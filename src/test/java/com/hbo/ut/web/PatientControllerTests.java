package com.hbo.ut.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hbo.ut.common.dto.PatientDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.nio.charset.StandardCharsets;
import java.time.LocalDate;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;


@SpringBootTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc(addFilters = false)
public class PatientControllerTests {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    private PatientDto patient;
    //private AppointmentDto appointment;

    @BeforeEach
    public void setUp() {
        patient = PatientDto.builder()
                .id(1)
                .firstname("John")
                .lastname("Doe")
                //.birthday(LocalDate.parse("11-08-1990", DateTimeFormatter.ofPattern("dd-MM-yyyy") )).securityNumber(40932)
                .birthday(LocalDate.of(1990, 8, 11))
                .securityNumber(40932)
                .build();
    }

    @Test
    public void when_create_then_return_created() throws Exception {
        mockMvc.perform(
                        post("/patients/")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(patient))
                                .characterEncoding(StandardCharsets.UTF_8)
                ).andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.content().json("1"));
    }

    @Test
    public void when_create_then_return_badRequest() throws Exception {
        mockMvc.perform(
                post("/patients/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("")
        ).andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void when_delete_then_not_null() throws Exception {
        mockMvc.perform(
                        delete("/patients/{id}", patient.getId())
                                .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.content().json("1"));
    }

    @Test
    public void when_findById_then_return_not_null() throws Exception {
        String response = mockMvc.perform(
                        post("/patients/")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(patient))
                ).andReturn()
                .getResponse()
                .getContentAsString();
        Integer patientId = objectMapper.readValue(response, Integer.class);
        mockMvc.perform(
                        get("/patients/{id}", patientId)
                ).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstname").value("John"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.birthday").value("11-08-1990"));
    }

    @Test
    public void when_findBySecurity_then_return_not_null() throws Exception {
        Integer secNumber = 2321;
        patient.setSecurityNumber(secNumber);
        mockMvc.perform(
                post("/patients/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(patient))
        );
        mockMvc.perform(
                        get("/patients/patient/{securityNumber}", secNumber)
                ).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstname").value("John"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.birthday").value("11-08-1990"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.securityNumber").value(patient.getSecurityNumber()));
    }

    @Test
    public void when_findAll_thenReturn_ok() throws Exception {
        mockMvc.perform(
                get("/patients/")
        ).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void when_findAppointments_thenReturn_ok() throws Exception {
        mockMvc.perform(
                get("/patients/{id}/appointments", patient.getId())
        ).andExpect(MockMvcResultMatchers.status().isOk());
    }

}
