package com.hbo.ut.service;

import com.hbo.ut.common.dto.AppointmentDto;
import com.hbo.ut.common.dto.PatientDto;

import java.util.Collection;

public interface PatientService extends CrudService<PatientDto, Integer> {

    PatientDto findBySecurityNumber(Integer securityNumber);

    Collection<AppointmentDto> findAppointments(Integer id);
}
