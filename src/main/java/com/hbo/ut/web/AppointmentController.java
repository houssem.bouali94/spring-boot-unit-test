package com.hbo.ut.web;

import com.hbo.ut.common.dto.AppointmentDto;
import com.hbo.ut.service.AppointmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/appointments")
@RequiredArgsConstructor
public class AppointmentController {

    private final AppointmentService appointmentService;

    @PostMapping("/")
    public ResponseEntity<Integer> create(@RequestBody AppointmentDto appointment) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(this.appointmentService.save(appointment));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Integer> delete(@PathVariable("id") Integer id) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(this.appointmentService.delete(id));
    }

    @GetMapping("/{id}")
    public ResponseEntity<AppointmentDto> find(@PathVariable("id") Integer id) {
        return ResponseEntity.ok(this.appointmentService.findById(id));
    }

    @GetMapping("/{medService}")
    public ResponseEntity<Collection<AppointmentDto>> find(@PathVariable("medService") String medicalService) {
        return ResponseEntity.ok(this.appointmentService.findByMedicalService(medicalService));
    }

    @GetMapping("/")
    public ResponseEntity<Collection<AppointmentDto>> find() {
        return ResponseEntity.ok(this.appointmentService.findAll());
    }
}
