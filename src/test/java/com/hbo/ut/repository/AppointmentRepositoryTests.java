package com.hbo.ut.repository;

import com.hbo.ut.domain.Appointment;
import com.hbo.ut.domain.Patient;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.InvalidDataAccessApiUsageException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.NoSuchElementException;
import java.util.Optional;

@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class AppointmentRepositoryTests {

    @Autowired
    private AppointmentRepository appointmentRepository;
    private Appointment appointment;
    private Patient patient;

    @BeforeEach
    public void setUp() {
        patient = Patient.builder()
                .firstname("John")
                .lastname("Doe")
                .birthday(LocalDate.of(1990, 8, 23))
                .securityNumber(40234)
                .build();

        appointment = Appointment.builder()
                .startDate(LocalDateTime.now())
                .endDate(LocalDateTime.now().plusMinutes(15)).medicalService("Emergency").patient(patient).build();
    }

    @Test
    public void when_save_null_then_throw_error() {
        Assertions.assertThatExceptionOfType(InvalidDataAccessApiUsageException.class)
                .isThrownBy(() -> appointmentRepository.save(null));
    }

    @Test
    public void when_save_then_return_not_null() {
        Appointment savedAppointment = appointmentRepository.save(appointment);

        Assertions.assertThat(savedAppointment).isNotNull();
        Assertions.assertThat(savedAppointment.getId()).isNotZero();
        Assertions.assertThat(savedAppointment.getPatient()).isNotNull();
    }

    @Test
    public void when_save_existing_appointment_then_update() {
        Appointment savedAppointment = appointmentRepository.save(appointment);
        savedAppointment.setMedicalService("Cardiovascular");

        Appointment updatedAppointment = appointmentRepository.save(savedAppointment);

        Assertions.assertThat(updatedAppointment).isNotNull();
        Assertions.assertThat(updatedAppointment.getMedicalService()).isEqualTo("Cardiovascular");
    }

    @Test
    public void when_findById_then_error() {
        Assertions.assertThatExceptionOfType(InvalidDataAccessApiUsageException.class)
                .isThrownBy(() -> appointmentRepository.findById(null));
    }

    @Test
    public void when_findById_then_return_not_null() {
        Appointment save = appointmentRepository.save(appointment);

        Optional<Appointment> byId = appointmentRepository.findById(save.getId());

        Assertions.assertThat(byId).isPresent();
        Assertions.assertThat(byId.get().getId()).isNotZero();
    }

    @Test
    public void when_findById_not_exist_then_return_null() {
        Optional<Appointment> byId = appointmentRepository.findById(0);

        Assertions.assertThat(byId).isEmpty();
        Assertions.assertThatExceptionOfType(NoSuchElementException.class)
                .isThrownBy(byId::get);
    }


}
