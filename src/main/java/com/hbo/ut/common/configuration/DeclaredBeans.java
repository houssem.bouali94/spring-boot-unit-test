package com.hbo.ut.common.configuration;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DeclaredBeans {

    @Bean
    public ModelMapper getModelMapper() {
        return new ModelMapper();
    }
}
