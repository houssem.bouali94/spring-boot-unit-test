package com.hbo.ut.service;

import com.hbo.ut.common.dto.AppointmentDto;

import java.util.Collection;

public interface AppointmentService extends CrudService<AppointmentDto, Integer> {

    Collection<AppointmentDto> findByMedicalService(String medicalService);
}
