package com.hbo.ut.repository;

import com.hbo.ut.domain.Patient;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDate;
import java.util.Optional;

@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class PatientRepositoryTests {

    @Autowired
    private PatientRepository repository;
    private Patient patient;

    @BeforeEach
    public void setUp() {
        patient = Patient.builder()
                .firstname("John")
                .lastname("Doe")
                .birthday(LocalDate.of(1990, 8, 23))
                .securityNumber(40234)
                .build();

    }

    @Test
    public void when_save_return_not_null() {
        Patient savedPatient = repository.save(patient);

        Assertions.assertThat(savedPatient).isNotNull();
        Assertions.assertThat(savedPatient.getId()).isGreaterThan(0);
        Assertions.assertThat(savedPatient.getBirthday()).isEqualTo("1990-08-23");
    }

    @Test
    public void when_update_return_not_null() {
        Patient savedPatient = repository.save(patient);

        savedPatient.setFirstname("Adam");
        savedPatient.setLastname("Smith");

        Patient updatedPatient = repository.save(savedPatient);

        Assertions.assertThat(updatedPatient).isNotNull();
        Assertions.assertThat(updatedPatient.getId()).isEqualTo(patient.getId());
        Assertions.assertThat(updatedPatient.getFirstname()).isEqualTo("Adam");
        Assertions.assertThat(updatedPatient.getLastname()).isEqualTo("Smith");
    }

    @Test
    public void when_delete_then_return_null() {
        Patient savedPatient = repository.save(patient);

        repository.deleteById(savedPatient.getId());
        Optional<Patient> byId = repository.findById(savedPatient.getId());

        Assertions.assertThat(byId.isPresent()).isFalse();
    }

    @Test
    public void when_findBySecurityNumber_then_not_null() {

        repository.save(patient);
        Optional<Patient> bySecurityNumber = repository.findBySecurityNumber(patient.getSecurityNumber());

        Assertions.assertThat(bySecurityNumber.isPresent()).isTrue();
        Assertions.assertThat(bySecurityNumber.get().getSecurityNumber()).isEqualTo(40234);
    }
}
