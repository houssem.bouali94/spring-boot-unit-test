package com.hbo.ut.service;

import com.hbo.ut.common.dto.AppointmentDto;
import com.hbo.ut.common.dto.PatientDto;
import com.hbo.ut.domain.Appointment;
import com.hbo.ut.domain.Patient;
import com.hbo.ut.repository.PatientRepository;
import com.hbo.ut.service.impl.PatientServiceImpl;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PatientServiceTests {

    @Mock
    private PatientRepository patientRepository;
    @Mock
    private ModelMapper modelMapper;
    @InjectMocks
    private PatientServiceImpl patientService;

    private PatientDto patient;
    private Patient patientEntity;
    private Appointment appointmentEntity;
    private AppointmentDto appointment;

    @BeforeEach
    public void setUp() {

        patient = PatientDto.builder()
                .id(1)
                .firstname("John")
                .lastname("Doe")
                .birthday(LocalDate.of(1990, 8, 11))
                .securityNumber(40932)
                //.appointments(Collections.singletonList(appointment))
                .build();
        patientEntity = Patient.builder()
                .id(1)
                .firstname("John")
                .lastname("Doe")
                .birthday(LocalDate.of(1990, 8, 11))
                .securityNumber(40932)
                //.appointments(Collections.singletonList(appointmentEntity))
                .build();

    }

    @Test
    public void when_saveNull_then_throw_error() {
        Assertions.assertThatIllegalArgumentException()
                .isThrownBy(() -> patientService.save(null));
    }

    @Test
    public void when_save_then_not_null() {
        when(modelMapper.map(patient, Patient.class))
                .thenReturn(patientEntity);
        when(patientRepository.save(Mockito.any(Patient.class)))
                .thenReturn(patientEntity);

        Integer id = patientService.save(patient);
        Assertions.assertThat(id).isNotNull();
        Assertions.assertThat(id).isNotZero();
    }

    @Test
    public void when_delete_with_null_then_throw() {
        //doNothing().when(patientRepository).deleteById(patient.getId());
        Assertions.assertThatIllegalArgumentException()
                .isThrownBy(() -> patientService.delete(null));
    }

    @Test
    public void when_delete_then_not_null() {
        doNothing().when(patientRepository).deleteById(patient.getId());
        Integer id = patientService.delete(patient.getId());
        Assertions.assertThat(id).isNotNull();
        Assertions.assertThat(id).isNotZero();
        Assertions.assertThat(id).isEqualTo(patient.getId());
    }

    @Test
    public void when_find_by_null_id_then_throw() {
        Assertions.assertThatIllegalArgumentException()
                .isThrownBy(() -> patientService.findById(null));
    }

    @Test
    public void when_find_by_id_is0_then_throw() {
        Assertions.assertThatIllegalArgumentException()
                .isThrownBy(() -> patientService.findById(0));
    }

    @Test
    public void when_findById_then_not_null() {
        when(patientRepository.findById(patient.getId()))
                .thenReturn(Optional.of(patientEntity));
        when(modelMapper.map(patientEntity, PatientDto.class))
                .thenReturn(patient);

        PatientDto patientById = patientService.findById(this.patient.getId());

        Assertions.assertThat(patientById).isNotNull();
        Assertions.assertThat(patientById.getId()).isEqualTo(patient.getId());
    }

    @Test
    public void when_findAll_then_return_not_empty() {
        Patient patient1 = Patient.builder()
                .id(2)
                .firstname("Salah")
                .lastname("Godbani")
                .build();
        Patient patient2 = Patient.builder()
                .id(3)
                .firstname("Ali")
                .lastname("Zitouni")
                .build();
        when(patientRepository.findAll())
                .thenReturn(Arrays.asList(patientEntity, patient1, patient2));

        Collection<PatientDto> patients = patientService.findAll();

        Assertions.assertThat(patients).isNotEmpty();
        Assertions.assertThat(patients.size()).isEqualTo(3);
    }

    @Test
    public void should_return_empty_when_findAll() {
        when(patientRepository.findAll())
                .thenReturn(Collections.emptyList());
        Collection<PatientDto> patients = patientService.findAll();
        Assertions.assertThat(patients).isNotNull();
        Assertions.assertThat(patients).isEmpty();
    }

    @Test
    public void should_throw_error_when_findBySecurityNumber_is_null() {
        Assertions.assertThatIllegalArgumentException()
                .isThrownBy(() -> patientService.findBySecurityNumber(null));
    }

    @Test
    public void should_throw_error_when_findBySecurityNumber_is0() {
        Assertions.assertThatIllegalArgumentException()
                .isThrownBy(() -> patientService.findBySecurityNumber(0));
    }

    @Test
    public void should_return_not_null_when_findBySecurityNumber() {
        when(patientRepository.findBySecurityNumber(patient.getSecurityNumber()))
                .thenReturn(Optional.of(patientEntity));
        when(modelMapper.map(patientEntity, PatientDto.class))
                .thenReturn(patient);

        PatientDto bySecurityNumber = patientService.findBySecurityNumber(patient.getSecurityNumber());

        Assertions.assertThat(bySecurityNumber).isNotNull();
    }

    @Test
    public void should_throw_error_when_findAppointments_with_null(){
        Assertions.assertThatIllegalArgumentException()
                .isThrownBy(() -> patientService.findAppointments(null));
    }

    @Test
    public void should_throw_error_when_findAppointments_with_0(){
        Assertions.assertThatIllegalArgumentException()
                .isThrownBy(() -> patientService.findAppointments(0));
    }

    @Test
    public void should_return_not_empty_when_findAppointments() {
        appointmentEntity = Appointment.builder()
                .id(4)
                .startDate(LocalDateTime.now())
                .endDate(LocalDateTime.now().plusMinutes(15))
                .medicalService("Cardio")
                .patient(patientEntity)
                .build();
        appointment = AppointmentDto.builder()
                .id(4)
                .startDate(LocalDateTime.now())
                .endDate(LocalDateTime.now().plusMinutes(15))
                .medicalService("Cardio")
                .patientId(patient.getId())
                .build();
        patient.setAppointments(Collections.singletonList(appointment));
        patientEntity.setAppointments(Collections.singletonList(appointmentEntity));

        when(patientRepository.findById(Mockito.anyInt()))
                .thenReturn(Optional.of(patientEntity));
        when(modelMapper.map(appointmentEntity, AppointmentDto.class))
                .thenReturn(appointment);
        List<AppointmentDto> appointments = (List<AppointmentDto>) patientService.findAppointments(patient.getId());

        Assertions.assertThat(appointments).isNotNull();
        Assertions.assertThat(appointments).isNotEmpty();
        Assertions.assertThat(appointments.size()).isEqualTo(1);
    }

    @Test
    public void should_throw_nullPointerException_when_findAppointments() {
        when(patientRepository.findById(Mockito.anyInt()))
                .thenReturn(Optional.of(patientEntity));

        Assertions.assertThatNullPointerException()
                .isThrownBy(() -> patientService.findAppointments(patient.getId()));
    }

    @Test
    public void should_contain_patientId_when_findAppointments() {
        appointmentEntity = Appointment.builder()
                .id(4)
                .startDate(LocalDateTime.now())
                .endDate(LocalDateTime.now().plusMinutes(15))
                .medicalService("Cardio")
                .patient(patientEntity)
                .build();
        appointment = AppointmentDto.builder()
                .id(4)
                .startDate(LocalDateTime.now())
                .endDate(LocalDateTime.now().plusMinutes(15))
                .medicalService("Cardio")
                .patientId(patient.getId())
                .build();
        patient.setAppointments(Collections.singletonList(appointment));
        patientEntity.setAppointments(Collections.singletonList(appointmentEntity));

        when(patientRepository.findById(Mockito.anyInt()))
                .thenReturn(Optional.of(patientEntity));
        when(modelMapper.map(appointmentEntity, AppointmentDto.class))
                .thenReturn(appointment);
        List<AppointmentDto> appointments = (List<AppointmentDto>) patientService.findAppointments(patient.getId());

        Assertions.assertThat(appointments.get(0).getPatientId()).isEqualTo(patient.getId());
    }
}
