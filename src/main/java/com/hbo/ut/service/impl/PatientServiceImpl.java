package com.hbo.ut.service.impl;

import com.hbo.ut.common.dto.AppointmentDto;
import com.hbo.ut.common.dto.PatientDto;
import com.hbo.ut.domain.Patient;
import com.hbo.ut.repository.PatientRepository;
import com.hbo.ut.service.PatientService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class PatientServiceImpl implements PatientService {

    private final PatientRepository patientRepository;
    private final ModelMapper modelMapper;

    @Override
    public Integer save(PatientDto patientDto) {
        if (patientDto == null) {
            log.error("Null parameter");
            throw new IllegalArgumentException("PatientDTO should not be null");
        }
        Patient patient = modelMapper.map(patientDto, Patient.class);
        return this.patientRepository.save(patient).getId();
    }

    @Override
    public Integer delete(Integer id) {
        if (id == null || id == 0) {
            log.error("Null ID value");
            throw new IllegalArgumentException("ID cannot be null or zero");
        }
        this.patientRepository.deleteById(id);
        return id;
    }

    @Override
    public PatientDto findById(Integer id) {
        if (id == null || id == 0) {
            log.error("Null ID value");
            throw new IllegalArgumentException("ID cannot be null or zero");
        }
        return this.patientRepository.findById(id)
                .map(patient -> modelMapper.map(patient, PatientDto.class))
                .orElse(null);
    }

    @Override
    public Collection<PatientDto> findAll() {
        return this.patientRepository.findAll()
                .stream()
                .map(patient -> modelMapper.map(patient, PatientDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public PatientDto findBySecurityNumber(Integer securityNumber) {
        if (securityNumber == null || securityNumber == 0) {
            log.error("Null ID value");
            throw new IllegalArgumentException("Security number cannot be null or zero");
        }
        return this.patientRepository.findBySecurityNumber(securityNumber)
                .map(patient -> modelMapper.map(patient, PatientDto.class))
                .orElse(null);
    }

    @Override
    public Collection<AppointmentDto> findAppointments(Integer id) {
        if (id == null || id == 0) {
            log.error("Null ID value");
            throw new IllegalArgumentException("Security number cannot be null or zero");
        }
        return this.patientRepository.findById(id)
                .map(patient -> patient.getAppointments()
                        .stream()
                        .map(appointment -> modelMapper.map(appointment, AppointmentDto.class))
                        .collect(Collectors.toList()))
                .orElse(Collections.emptyList());
    }
}
