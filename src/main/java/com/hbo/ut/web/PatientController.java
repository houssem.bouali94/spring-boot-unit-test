package com.hbo.ut.web;

import com.hbo.ut.common.dto.AppointmentDto;
import com.hbo.ut.common.dto.PatientDto;
import com.hbo.ut.service.PatientService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/patients")
@RequiredArgsConstructor
public class PatientController {

    private final PatientService patientService;

    @PostMapping("/")
    public ResponseEntity<Integer> create(@RequestBody PatientDto patient) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(this.patientService.save(patient));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Integer> delete(@PathVariable("id") Integer id) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(this.patientService.delete(id));
    }

    @GetMapping("/{id}")
    public ResponseEntity<PatientDto> find(@PathVariable("id") Integer id) {
        return ResponseEntity.ok(this.patientService.findById(id));
    }

    @GetMapping("/")
    public ResponseEntity<Collection<PatientDto>> find() {
        return ResponseEntity.ok(this.patientService.findAll());
    }

    @GetMapping("/patient/{securityNumber}")
    public ResponseEntity<PatientDto> findBySecurityNumber(@PathVariable("securityNumber") Integer securityNumber) {
        return ResponseEntity.ok(this.patientService.findBySecurityNumber(securityNumber));
    }

    @GetMapping("/{id}/appointments")
    public ResponseEntity<Collection<AppointmentDto>> findAppointments(@PathVariable("id") Integer securityNumber) {
        return ResponseEntity.ok(this.patientService.findAppointments(securityNumber));
    }
}
