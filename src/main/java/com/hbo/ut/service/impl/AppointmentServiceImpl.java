package com.hbo.ut.service.impl;

import com.hbo.ut.common.dto.AppointmentDto;
import com.hbo.ut.common.dto.PatientDto;
import com.hbo.ut.domain.Appointment;
import com.hbo.ut.domain.Patient;
import com.hbo.ut.repository.AppointmentRepository;
import com.hbo.ut.repository.PatientRepository;
import com.hbo.ut.service.AppointmentService;
import com.hbo.ut.service.PatientService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class AppointmentServiceImpl implements AppointmentService {

    private final AppointmentRepository appointmentRepository;
    private final PatientRepository patientRepository;
    private final ModelMapper modelMapper;

    @Override
    public Integer save(AppointmentDto appointmentDto) {
        if (appointmentDto == null) {
            log.error("Null parameter");
            throw new IllegalArgumentException("AppointmentDto should not be null");
        }
        Integer patientId = appointmentDto.getPatientId();
        if (patientId == null || patientId == 0) {
            log.error("Null patient ID");
            throw new IllegalArgumentException("Appointment should have a patient ID");
        }
        Patient patient = this.patientRepository.findById(patientId)
                .orElseThrow(() -> new IllegalStateException("No patient found could not proceed"));
        Appointment appointment = modelMapper.map(appointmentDto, Appointment.class);
        appointment.setPatient(patient);
        return this.appointmentRepository.save(appointment).getId();
    }

    @Override
    public Integer delete(Integer id) {
        if (id == null || id == 0) {
            log.error("Null ID value");
            throw new IllegalArgumentException("ID cannot be null or zero");
        }
        this.appointmentRepository.deleteById(id);
        return id;
    }

    @Override
    public AppointmentDto findById(Integer id) {
        if (id == null || id == 0) {
            log.error("Null ID value");
            throw new IllegalArgumentException("ID cannot be null or zero");
        }
        Appointment appointment = this.appointmentRepository.findById(id).orElse(null);
        return modelMapper.map(appointment, AppointmentDto.class);
    }

    @Override
    public Collection<AppointmentDto> findAll() {
        return this.appointmentRepository.findAll()
                .stream()
                .map(appointment -> modelMapper.map(appointment, AppointmentDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public Collection<AppointmentDto> findByMedicalService(String medicalService) {
        if (medicalService.isBlank()) {
            log.error("Null ID value");
            throw new IllegalArgumentException("Medical Service cannot be empty");
        }
        return this.appointmentRepository.findByMedicalService(medicalService)
                .stream()
                .map(appointment -> modelMapper.map(appointment, AppointmentDto.class))
                .collect(Collectors.toList());
    }
}
